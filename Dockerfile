FROM centos:7

ARG agentfile

WORKDIR "/root/stonebranch"

ADD ${agentfile} /root/stonebranch/

ADD ucmd /etc/pam.d/

RUN tar -xvzf /root/stonebranch/${agentfile} -C /root/stonebranch

CMD sh /root/stonebranch/unvinst --network_provider oms \
                                 --oms_servers $oms_servers \
                                 --oms_port $oms_port \
                                 --oms_autostart $oms_autostart \
                                 --ac_netname $ac_netname \
                                 --opscli $opscli \
                                 --user ubroker \
                                 --create_user yes \
         && tail -f /dev/null
